## Laravel ##
This project is the first time I work with laravel framerwork. 
### Note
This project need to connect mongo DB, config at /config/database.php 
### Installation ###

* `git clone https://gitlab.com/huutuan/web_report_laravel.git`
* `cd projectname`
* `composer install`
* `composer require jenssegers/mongodb `
* Create inform *.env*
* `php artisan key:generate`
* `php artisan serve` to start the app on http://localhost:8000/


