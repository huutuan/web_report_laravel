<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Logs extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'logs';
    protected $primarykey = '_id';

    public function user(){
        return $this->belongsto("App\User");
    }

}
