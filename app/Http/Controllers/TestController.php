<?php


namespace App\Http\Controllers;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Http\Request;
use App\User;
use App\Logs;
use App\User_all;

class TestController extends Controller
{
    public function index(){
        $data_users = User::all();
        $logs_first = Logs::where('idslack', $data_users[0]->IdSlack)->get();
        return view('index', ["data_users" => $data_users, "logs_first" => ($logs_first)]);
        
    }


    public function getLogs($id) {
        $logs = Logs::where('idslack', $id)->get();

        return json_encode($logs);
    }

    public function searchdate(Request $req, $idslack){
        $data = $req->all();
        $logs = Logs::where('idslack', $idslack)->get();
        $data_users = User::where('idslack', $idslack)->get();
        $allDays = [];
        foreach($logs[0]->logs as $key=>$value) {
            foreach($value as $k=>$val){
                if (($k >= $req->from_date) && ($k <= $req->to_date)) {
                    array_push($allDays, [$k => $val]);

                }
            }

           
        }
        return json_encode($allDays);
    }

    public function getdata(){
        $data = User_all::all();
        return json_encode($data);
    }
}
