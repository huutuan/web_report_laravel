<?php

namespace App\Http\Controllers;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Http\Request;
use App\User;
use App\Logs;
use Auth;
use Illuminate\Support\MessageBag;
use Validator;


class LoginController extends Controller
{
    
    public function getLogin() {
		return view('login');
    }
    public function postLogin(Request $request) {

    	$rules = [
    		'username' =>'required',
    		'password' => 'required'
    	];
        $validator = Validator::make($request->all(), $rules);

        $username = trim($request->input('username'));
		$password = trim($request->input('password'));

        //get user data 
        $data = file_get_contents(base_path('database/user_auth.json'));
        $data = json_decode($data, true);
        // dd($data);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            $waiter = 0;    //incorect login
            foreach ($data as $pair){
                if ($username !== (string)$pair['username'] || $password !== (string)$pair['password']){
                    continue;
                    // dd($pair['username'], $pair['password']);
                }
                else{
                    $waiter = 1;
                };
            }
            if ($waiter == 1){
                return redirect('/main');
            }
            else{
                $errors = new MessageBag(['errorlogin' => 'username or password incorrect Please try again OR contact tuanlh to solve']);
                return redirect()->back()->withInput()->withErrors($errors);
            };
        
        };

	}

	public function redirectToGoogleProvider(){
    	$parameters = ['access_type' => 'offline'];
    	return \Socialite::driver('google')->scopes(["https://www.googleapis.com/auth/drive"])->with($parameters)->redirect();
    }
     
    public function handleProviderGoogleCallback()
    {

    	$auth_user = \Socialite::driver('google')->stateless()->user();
    	$user = User::updateOrCreate(['email' => $auth_user->email], ['refresh_token' => $auth_user->token, 'name' => $auth_user->name]);
    	Auth::login($user, true);
    	return redirect()->to('/'); // Redirect to a secure page
    }
}
