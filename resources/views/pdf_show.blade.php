@extends('layout.master')
@section('content')

<div class="main-panel">
  <div class="content-wrapper" style="margin-top:7%;">
    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">      
            <div class="container"> 
                <object id="pdfviewer" data="" type="application/pdf" style="width:100%;height:500px;"></object>

            </div>
        </div>
      </div>  
			
    </div>
</div> 

@endsection

@section('inline_scripts')
<script src="../../../assets/js/js/google_api.js"></script>
@endsection 