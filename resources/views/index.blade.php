@extends('layout.master')
@section('content')

<div class="main-panel">
  <div class="content-wrapper" style="margin-top:7%;">
    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">      
            <div class="container"> 
            <h5>Chọn số ngày muốn hiện thị:</h5>
              <div class="form-group">
                <select class  ="form-control" name="state" id="maxRows">
                  <option value="5000">Show ALL Rows</option>
                  <option value="5">5</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                  <option value="20">20</option>
                  <option value="50">50</option>
                  <option value="70">70</option>
                  <option value="100">100</option>
                </select>     
                </div>
        
              <div id="container">
              <p class='get_id' hidden>{{$data_users[0]->IdSlack}}</p>
              <h2>{{$data_users[0]->LastName}} {{$data_users[0]->MiddleName}} {{$data_users[0]->FirstName}}</h2>
              <div id='order_table'> 
                <table  class='table table-striped table-class col-md-12' id='table-id'>
                  <tr>
                    <th> Thứ </th>
                    <th> Hôm qua </th>
                    <th> Hôm nay </th>
                    <th> Khó khắn </th>
                  </tr> 
                  @foreach($logs_first as $log_first)
                    @foreach($log_first->logs as $key=>$value)
                    <tr>
                      @foreach($value as $keyDate=>$valueDate)
                      <td>{{$keyDate}}</td>
                        @foreach($valueDate as $k=>$v)
                        @if($k == "pre-day")
                        <td>{{$v["message"]}}</td>
                        @endif
                        @if($k == "current-day")
                        <td>{{$v["message"]}}</td>
                        @endif
                        @if($k == "challenge")
                        <td>{{$v["message"]}}</td>
                        @endif
                      @endforeach
                      @endforeach
                      </tr>
                    @endforeach
                  
                  @endforeach
              
                    </table>
                  </div>
              </div>
            </div>
            <div class='pagination-container' >
              <nav>
                <ul class="pagination">            
                  <li data-page="prev" >
                    <span> < <span class="sr-only">(current)</span></span>
                  </li>
                  <li data-page="next" id="prev">
                    <span> > <span class="sr-only">(current)</span></span>
                  </li>
                </ul>
                <button class="btn-pdf"> Newest Report Downloader</button>
                <button class="btn-view"> Newest Report Viewer</button>
              </nav>
			      </div>
          </div>
        </div>
      </div>  
			
    </div>
</div> 

@endsection

@section('inline_scripts')
<script src="../../../assets/js/js/google_api.js"></script>
@endsection 