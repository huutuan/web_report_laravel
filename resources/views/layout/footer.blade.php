<script src="../../../assets/vendors/js/vendor.bundle.base.js"></script>
<script src="../../../assets/vendors/js/vendor.bundle.addons.js"></script>
<script src="../../../assets/js/shared/off-canvas.js"></script>
<script src="../../../assets/js/shared/misc.js"></script>
<script src="../../../assets/js/js/pagination_table.js"></script>
<script src="../../../assets/js/js/checkbox.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>




<script>
    function searchName() {
    var input, filter, ul, li, i, txtValue;
    input = document.getElementById("searchName");
    filter = input.value.toUpperCase();
    ul = document.getElementById("listName");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
    }

    $(document).ready(function() {
        $(".nav-item-name").click(function() {
            var selectedId = $(this).attr("id");
            $("#filter").data("idslack", selectedId);
            var username = $(this).data("username");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/users/' + selectedId + '/getLogs',
                data: selectedId,
                type: 'post',
                success: function(result) {
                    var logsInfo = JSON.parse(result);
                    var date = [];
                    var pre_day = [];
                    var current_day = [];
                    var challenge = [];
                    for (i = 0; i < logsInfo.length; i++) {
                        var logs = logsInfo[i].logs
                        for (i = 0; i < logs.length; i++) {
                            Object.entries(logs[i]).forEach(([datekey, datevalue]) => {
                                date.push(datekey);
                                Object.entries(datevalue).forEach(([key, value]) => {
                                    if (key == "pre-day") {
                                        pre_day.push(value.message)
                                    } else if (key == "current-day") {
                                        current_day.push(value.message)
                                    } else if (key == "challenge") {
                                        challenge.push(value.message)

                                    };
                                });

                            });
                        };
                    };
                    var newTableHtml = date;
                    $("table").empty()
                    $("table").append(newTableHtml);
                    var html = "<p class='get_id' hidden>"+selectedId+"</p>"
                    html += "<h2 id='check_name'>"  + username + "</h2><div id='order_table'><table  class='table table-striped table-class col-md-12' id='table-id'><tr>";
                    html += "<th>" + "Thứ" + "</th>";
                    html += "<th>" + "Hôm qua" + "</th>";
                    html += "<th>" + "Hôm nay" + "</th>";
                    html += "<th>" + "Khó khắn" + "</th>";
                    html += "</tr><tr>"
                    for (var i = 0; i < newTableHtml.length; i++) {
                        html += "<td>" + date[i] + "</td>";
                        html += "<td>" + pre_day[i] + "</td>";
                        html += "<td>" + current_day[i] + "</td>";
                        html += "<td>" + challenge[i] + "</td></tr>";
                        var next = i + 1;
                    }
                    html += "</table></div>";
                    document.getElementById("container").innerHTML = html;
                }
            });




            $.datepicker.setDefaults({
                dateFormat: 'yy-mm-dd'
            });
            $(function() {
                $("#from_date").datepicker();
                $("#to_date").datepicker();
            });
        });


        $('#filter').click(function() {
            console.log($('#filter').data('idslack'));
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var idslack = $('#filter').data('idslack');

            if (from_date != '' && to_date != '') {
                $.ajax({
                    url: "/users/" + idslack + "/getLogs/searchDate",
                    method: "POST",
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    },
                    success: function(responseData) {
                        var DateInfo = JSON.parse(responseData);
                        var search_date = [];
                        var search_pre_day=[];
                        var search_current_day = [];
                        var search_challenge = [];
                        for (i = 0; i < DateInfo.length; i++) {
                            Object.entries(DateInfo[i]).forEach(([date_key, date_value]) => {
                                search_date.push(date_key);
                                Object.entries(date_value).forEach(([key_s, value_s]) => {
                                    if (key_s == "pre-day") {
                                        search_pre_day.push(value_s.message)
                                    } else if (key_s == "current-day") {
                                        search_current_day.push(value_s.message)
                                    } else if (key_s == "challenge") {
                                        search_challenge.push(value_s.message)

                                    };
                                });
                              
                            });
                            
                        }

                        var newTableHtmlSearch = search_date;
                        $("table").empty()
                        $("table").append(newTableHtmlSearch);
                        var html = "<div id='order_table'><table  class='table table-striped table-class col-md-12' id='table-id'><tr>";
                        html += "<th>" + "Thứ" + "</th>";
                        html += "<th>" + "Hôm qua" + "</th>";
                        html += "<th>" + "Hôm nay" + "</th>";
                        html += "<th>" + "Khó khăn" + "</th>";
                        html += "</tr><tr>"
                        for (var i = 0; i < newTableHtmlSearch.length; i++) {
                            html += "<td>" + search_date[i] + "</td>";
                            html += "<td>" + search_pre_day[i] + "</td>";
                            html += "<td>" + search_current_day[i] + "</td>";
                            html += "<td>" + search_challenge[i] + "</td></tr>";
                            var next = i + 1;
                        }
                        html += "</table></div>";
                        document.getElementById("order_table").innerHTML = html;
                        // $('#order_table').html(data);
                    }
                });
            } else {
                alert("Please Select Date");
            }
        });
    });
</script>