<div class="container-scroller">
      <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
          <a class="navbar-brand brand-logo" href="../../index.html">
            <img src="../../../assets/images/logo.svg" alt="logo" /> </a>
          <a class="navbar-brand brand-logo-mini" href="../../index.html">
            <img src="../../../assets/images/logo-mini.svg" alt="logo" /> </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
        <input type="text" name="from_date" id="from_date" class="form-control" placeholder="Từ ngày" />
        <input type="text" name="to_date" id="to_date" class="form-control" placeholder="Đến ngày" /> 
        <input type="button" name="filter" id="filter" value="Tìm" class="btn btn-info" />  
        <ul class="navbar-nav ml-auto"> 
            <li class="nav-item dropdown d-none d-xl-inline-block user-dropdown">
              <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <img class="img-xs rounded-circle" src="../../../assets/images/faces/face8.jpg" alt="Profile image"> </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <div class="dropdown-header text-center">
                  <img class="img-md rounded-circle" src="../../../assets/images/faces/face8.jpg" alt="Profile image">
                  <p class="mb-1 mt-3 font-weight-semibold">Allen Moreno</p>
                  <p class="font-weight-light text-muted mb-0">allenmoreno@gmail.com</p>
                </div>
                <a class="dropdown-item">Sign Out<i class="dropdown-item-icon ti-power-off"></i></a>
              </div>
            </li>
          </ul>
          </div>      
         
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>
      <div class="container-fluid page-body-wrapper">
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <input type="text" id="searchName" onkeyup="searchName()" placeholder="Tìm tên" title="Type in a name">
          <ul class="nav" id="listName">
           
            @foreach($data_users as $data_user)            
              <li class="nav-item">
              <a href="#">
                <button class="nav-link nav-item-name" id='{{$data_user->IdSlack}}' data-username="{{$data_user->LastName}} {{$data_user->MiddleName}} {{$data_user->FirstName}}">
                  <i class="menu-icon typcn typcn-document-text"></i>
                  <span class="menu-title">{{$data_user->LastName}} {{$data_user->MiddleName}} {{$data_user->FirstName}}</span>
                </button>
                </a>
              </li>
            @endforeach         
          </ul>
          <form class="checkbox">
            <div class="multiselect">
              <div class="selectBox" onclick="showCheckboxes()">
                <select>
                  <option class="mdi mdi-filter text-danger">Chọn</option>
                </select>
                <div class="overSelect"></div>
              </div>
              <div id="checkboxes">
                <label for="one">
                  <input type="checkbox" id="one" />Nhóm 1</label>
                <label for="two">
                  <input type="checkbox" id="two" />Nhóm 2</label>
                <label for="three">
                  <input type="checkbox" id="three" />Nhóm 3</label>
              </div>
            </div>
          </form>
          
              </span>
          <div id="selection-table">

        
        
      </div>
        </nav>