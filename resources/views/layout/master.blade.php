<!DOCTYPE html>
<html lang="en">
@include('layout.head')
@yield('inline_styles')
<body>
<div class="full-size">
       @include('layout.header')
        <!--Content-->
        @yield('content')
        <!--./Content -->
        @include('layout.footer')
        @yield('inline_scripts')
</div>
</body>
</html>
