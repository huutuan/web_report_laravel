FROM lorisleiva/laravel-docker:7.3

COPY composer.lock composer.json /var/www/

COPY database /var/www/database

WORKDIR /var/www

RUN apk update && apk add autoconf openssl-dev php-mbstring php-pear
RUN pecl install mongodb && echo "extension=mongodb.so" > /usr/local/etc/php/conf.d/docker-php-ext-mongodb.ini
RUN composer install --no-scripts && composer require jenssegers/mongodb --no-scripts

COPY . /var/www

RUN mkdir -p /code/storage/logs /code/storage/framework/sessions && \
	chown -R www-data:www-data /var/www/storage /var/www/bootstrap/cache && \
	cp .env.example .env

RUN php artisan package:discover && \
	php artisan key:generate && \
	php artisan config:clear && \
	php artisan cache:clear

ENTRYPOINT ["bash", "serve.sh"]

