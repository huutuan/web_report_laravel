#!/bin/bash

sed -i -re "s/^DB_HOST=.*/DB_HOST=$DB_HOST/" \
	-e "s/^DB_PORT=.*/DB_PORT=$DB_PORT/" \
	-e "s/^DB_DATABASE=.*/DB_DATABASE=$DB_DATABASE/" \
	-e "s/^DB_USERNAME=.*/DB_USERNAME=$DB_USERNAME/" \
	-e "s/^DB_PASSWORD=.*/DB_PASSWORD=$DB_PASSWORD/" .env

php artisan serve --host=0.0.0.0
